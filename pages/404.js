import { useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

const NotFound = () => {

  const router = useRouter()

  useEffect(() => {
    setTimeout(() => {
      router.push("/")
    }, 3000)
  }, [])
  return (
    <div className="not-found">
      <h1>404</h1>
      <h2>Page Cannot found</h2>
      <p>Redirecting to <Link href="/">homepage</Link> ... </p>
      <style jsx>
        {`
        .not-found {
          background: #fff;
          padding: 30px;
          box-shadow: 1px 3px 5px rgba(0,0,0,0.1);
          transform: rotateZ(-1deg);
          txt-align: center;
        }
        h1 {
          font-size: 3em;
          text-align: center;
        }
        h2 {
          text-align: center;
        }
        p {
          text-align: center;
        }
        `}
      </style>
    </div>
  )
}

export default NotFound

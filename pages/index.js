import { createClient } from 'contentful'
import RecipeCard from '.././components/RecipeCard'

export const getStaticProps = async () => {
  const client = createClient({
    space: process.env.NEXT_CONTENTFUL_SPACE_ID,
    accessToken: process.env.NEXT_CONTENTFUL_DELIVERY_API
  }) // make connection to contenful space

  const res = await client.getEntries({ content_type: 'recipe' })

  return {
    props: {
      recipes: res.items
    },
    revalidate: 1 //regenerate and query data
  }
}

export default function Recipes({ recipes }) {
  console.log(recipes)
  return (
    <div className="recipe-list">
      {/* Recipe List */}
      {recipes.length > 0 && recipes.map((recipe, idx) => (
        <RecipeCard key={idx} recipe={ recipe }/>
      ))}

      {/* these styles only apply inside this component  */}

      <style jsx>{`
        .recipe-list {
          display:grid;
          grid-template-columns: 1fr 1fr;
          grid-gap: 20px 60px;
        }
      ` }</style>
    </div>
  )
}
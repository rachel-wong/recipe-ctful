import { createClient } from 'contentful'
import Image from 'next/image'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import Skeleton from '../../components/Skeleton'

// // this will be used in two separate functions so it's outside of the component
const client = createClient({
  space: process.env.NEXT_CONTENTFUL_SPACE_ID,
  accessToken: process.env.NEXT_CONTENTFUL_DELIVERY_API,
})

// //get all of the paths that would use this component
// // find every slug in contentful
// // make this to generate the paths
export const getStaticPaths = async () => {
  const res = await client.getEntries({
    content_type: "recipe"
  })

  const paths = res.items.map(item => {
    return {
      params: { slug: item.fields.slug }
    }
  })

  return {
    paths,
    fallback: true // if go to path doesn't exist, false show 404 page instead of fallback page
  }
}
// // fetch single item based on page we're on and injected as a prop
// // generate the data for the one slug
// if the data is new, it re runs this method to get the data after the
export const getStaticProps = async ({ params }) => {
  const { items } = await client.getEntries({
    content_type: 'recipe',
    'fields.slug': params.slug
  })

  // if items doens't have length (there is no data), then redirect elsewhere
  if (!items.length) {
    return {
      redirect: {
        destination: '/',
        permanent: false // not permanent in the sense that the slug might be used by future content so it should behave normally then
      }
    }
  }

  return {
    props: { recipe: items[0] }, // only pass in one recipe object
    revalidate: 1 // at most query data for updates in 1 second
  }

}

export default function RecipeDetails({ recipe }) {
  if (!recipe) return <Skeleton /> // error handling when new content is not available
  const {
    cookingTime,
    featureImage,
    ingredients,
    method,
    slug,
    title
  } = recipe.fields

  return (
    <div>
      <div className="banner">
        <Image className="banner-image"
          src={'https:' + featureImage.fields.file.url}
          width={featureImage?.fields?.file?.details?.image?.width}
          height={featureImage?.fields?.file?.details?.image?.height}
          alt={ title }
        />
        <h2>{ title }</h2>
      </div>
      <div className="info">
        <p>Take about {cookingTime} minutes to cook </p>
        <h3>Ingredients:</h3>
        {ingredients.length > 0 && ingredients.map((ing, idx) => (
          <span key={idx}>{ ing }</span>
        ))}
        <div className="method">
          <h3>Method:</h3>
          <div>{ documentToReactComponents(method)}</div>
        </div>
      </div>
      <style jsx>
        {`
          h2,h3 {
          text-transform: uppercase;
        }
        .banner h2 {
          margin: 0;
          background: #fff;
          display: inline-block;
          padding: 20px;
          position: relative;
          top: -60px;
          left: -10px;
          transform: rotateZ(-1deg);
          box-shadow: 1px 3px 5px rgba(0,0,0,0.1);
        }
        .banner div {
          min-width: 100% !important;
        }

        .banner-image {
          object-fit: cover !important;
        }
        .info p {
          margin: 0;
        }
        .info span::after {
          content: ", ";
        }
        .info span:last-child::after {
          content: ".";
        }
        `}
      </style>
    </div>
  )
}
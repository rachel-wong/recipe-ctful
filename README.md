## Recipe-Contentful app

> Deployed app: https://recipe-ctful.vercel.app/

![screenshot](/public/screenshot.gif)

#### Additional installs
* contentful (client)
* rich-text-react-renderer

#### Contentful connection

* need to get the space ID and content delivery api key from the contentful settings
* initialise an instance of the contentful client and use `getStaticProps` to make the call

#### Images
* Images are tricky because we need to whitelist the domain from which contentful automatically hosts the images. just using nextjs's image out of the box won't work. Instead we need to whitelist the domain which the image is being hosted

```javascript
module.exports = {
  images: {
    domains: ['example.com'],
  },
}
```

https://nextjs.org/docs/basic-features/image-optimization

1. create a next.config.js in the root
2. find the host name by console-logging out the data from contentful. under `thumbanil`, you should see the url for the image is being hosted at something like this:

```
//images.ctfassets.net/1d3mfpiskkff/1pNLIiPffw17r7S8jYMh5X/28813e428b12fb2dee4004213c52a004/chad-montano-lP5MCM6nZ5A-unsplash.jpeg
```

The domain to white list is therefore `images.ctfassets.net`

3. in the next.config.js, whitelist that domain with

```javascript
module.exports = {
  images: {
    domains: ['images.ctfassets.net'],
  },
```

4. restart the server

* interestingly I could still use the <img> to render out the images as per usual :/ THough using nextJS's image component helps with responsiveness and lazy loading.

### Style JSX
* adding the below tag inside a component will scope styles to that component. it's the equivalent of inline-styles, similar to modular styles except it's all in one file.

```jsx
<style jsx>
{`
  display:flex;
  justify-content: center;
  align-items: center;
`}
</style>
```

### Rich text blocks


### Incremental Static Regeneration
* changes in the contentful backend to be replicated across to the deployed version (regenerating on the fly)
* need to be added on a page-by-page basis
* only one line change at the file that would have data updates in the contentful backend

```jsx

export const getStaticProps = async() => {

  /..../

  return {
    props: {
      recipes: res.items
    },
    revalidate: 1 // <===== regenerate and query data
  }
}
```
The regenerating is only for amending existing content, not when new content is added (i.e. a new recipe). A fallback page is needed in these instances while nextjs fetches the content in the background.

## Fallback page
* placeholder until nextjs fetches new content (not 404).
* once data is fetched, then nextjs injects it into the page in real time after a few seconds of loading.

1. change `fallback` to true

```jsx
  export const getStaticPaths = async )( => {

    /***/

    return {
      paths,
      fallback: true // if go to path doesn't exist, false show 404 page instead of fallback page
  }
  }
```
2. Add a skeleton component to display while data is being pulled in (a series of shaded lines)
3. Add error handling to go to skeleton when there is no data in teh component

```javascript
  if (!recipe) return <Skeleton /> // error handling when new content is not available
```

* if data realy doesn't exist, then it shoudl redirect somehwere else (home page or 404)

### Conditional redirects

* if there is no data coming back then it should redirect someplace else
* add the check to getStaticProps before it hydrates the component

```jsx

export const getStaticProps = async () => {
  /***/
  if (!items.length) {
    return {
      redirect: {
        destination: '/',
        permanent: false // not permanent in the sense that the slug might be used by future content so it should behave normally then
      }
    }
  }
}

```

### Webhook/Deploy Hook

* accomplishes the same goal as incremental static regeneration except that when there is any changes to contentful at all, vercel is triggered to redeploy the entire app, not just the section that has been updated. This might be overkill if the changes are frequent and small.
* webhook is configured with contentful. deployhook is configured with vercel. when changes are made on contentful, webhook triggers the deployhook to re-deploy the whole entire application.

Getting vercel deployhookd
* go to project > settings > Git
* Create new deployhook. label it however you like. generate and copy the api endpoints

Getting the contentful webhook
* contentful > settings > webhook
* select the vercel template
* paste in teh deployhook
* go through the webhook settings to make sure all the options that you want it triggered are selected (i.e. create, modify, delete)

https://www.youtube.com/watch?v=x_jhDDPV2Ak&ab_channel=TheNetNinja